﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static DebugLog.DebugLog;
using static Conf.Conf;
using System.Security;
using System.Text;
using System.IO;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;
using static TcGitBar.FrmMain.Side;
using Microsoft.Win32;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Diagnostics;
using System.Linq;

namespace TcGitBar
{
    public partial class FrmMain : Form
    {
        private readonly string _gitExe;
        private readonly string _extToolExe;

        private const string CONF_KEY_GIT_EXE = "GitExecutablePath";
        private const string CONF_KEY_EXT_TOOL_EXE = "ExternalToolPath";

        private readonly Utils utils = new Utils();

        public FrmMain()
        {
            InitializeComponent();

            _timer.Interval = (int)(numPeriod.Value * 1000);
            _timer.Tick += new System.EventHandler(OnTimerTick);
            _timer.Enabled = true;

            _gitExe = ConfGet(CONF_KEY_GIT_EXE);
            _extToolExe = ConfGet(CONF_KEY_EXT_TOOL_EXE);

            InitializeEvents();

            InitApp();
        }

        private void InitApp()
        {
            string userCmdFilePath = GetTcUserCmdFilePath();

            string text = File.ReadAllText(userCmdFilePath);

            string userCmd =
$@"[em_git_status_right]
button=
cmd=C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe
param=/command:log /path:%X%T
[em_git_status_left]
button=
cmd=C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe
param=/command:log /path:%X%P
";

            if (text.Contains("[em_git_status_right]"))
            {
                string oldUserCmd =
@"\[em_git_status_right\]
.+
\[em_git_status_left\]
.+
param.+?\r\n
";
                text = Regex.Replace(text, oldUserCmd, userCmd, RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
            }
            else
            {
                text += userCmd;
            }

            File.WriteAllText(userCmdFilePath, text);
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            IntPtr hWnd = GetTcWindowHandle();
            if (hWnd == IntPtr.Zero)
                return;

            bool refresh = false;

            //get git repo params for left and right folder
            foreach (TCPanel panel in _panels.Values)
            {
                //get folder path
                Side side = _panels.First(x => x.Value == panel).Key;
                var path = GetTcPanelPath(side);
                if (panel.path != path)
                    refresh = true;
                panel.path = path;
                Dbg($"PATH {path}");

                if (!Directory.Exists(path))
                {
                    panel.Reset();
                    panel.path = path;
                    continue;
                }

                string branch = GitGetBranch(path);
                Dbg($"BRANCH '{branch}'");

                if (panel.branch != branch)
                    refresh = true;
                panel.branch = branch;

                if (branch == "")
                {
                    panel.Reset();
                    panel.path = path;
                    continue;
                }

                string modFiles = GitGetModifiedFiles(path);
                if (panel.modFiles != modFiles)
                    refresh = true;
                panel.modFiles = modFiles;

                string[] modFilesArray = GitGetModifiedFilesArray(modFiles);
                if (panel.modFilesCount != modFilesArray.Length)
                    refresh = true;
                panel.modFilesCount = modFilesArray.Length;
                panel.modFilesArray = modFilesArray;

                string remote = GitGetRemote(path);

#warning TEMPORARY W/A
                //in case we have more than one remote leave only last one
                //this must be fixed properly by creating MENUITEM for each remote
                remote = Regex.Replace(remote, ".+\n", "");
                
                if (panel.remote != remote)
                    refresh = true;
                panel.remote = remote;
            } //for LEFT and RIGHT

            if (!refresh)
                return;

            //aliases
            var leftPanel = _panels[Side.LEFT];
            var rightPanel = _panels[Side.RIGHT];

            //left branch name with modified file count
            string leftBranch = leftPanel.branch == "" ? NOGITSTR : PathGetTail(leftPanel.path) + " " + leftPanel.branch;
            if (leftPanel.modFilesCount > 0)
                leftBranch += " " + leftPanel.modFilesCount.ToString();

            //right branch name with modified file count
            string rightBranch = rightPanel.branch == "" ? NOGITSTR : PathGetTail(rightPanel.path) + " " + rightPanel.branch;
            if (rightPanel.modFilesCount > 0)
                rightBranch += " " + rightPanel.modFilesCount.ToString();

            //make menu items from left list of files
            string[] leftModFilesAr = leftPanel.modFilesArray;
            if (leftModFilesAr.Length > 20)
            {
                Array.Resize(ref leftModFilesAr, 20);
                leftModFilesAr[leftModFilesAr.Length - 1] = "...";
            }
            string leftFileMenu = "";
            Array.ForEach(leftModFilesAr, (f) => leftFileMenu += $"MENUITEM \"{f}\", em_git_status_left\r\n");

            //make menu items from right list of files
            string[] rightModFilesAr = rightPanel.modFilesArray;
            if (rightModFilesAr.Length > 20)
            {
                Array.Resize(ref rightModFilesAr, 20);
                rightModFilesAr[rightModFilesAr.Length - 1] = "...";
            }
            string rightFileMenu = "";
            Array.ForEach(rightModFilesAr, (f) => rightFileMenu += $"MENUITEM \"{f}\", em_git_status_right\r\n");

            //repo remote
            string leftRemote = leftPanel.remote.Replace('\t', ' ');
            string rightRemote = rightPanel.remote.Replace('\t', ' ');

            //build the menu
            string search = @"STARTMENU.*HELP_BREAK";
            string replace = $@"STARTMENU

POPUP ""         ""
END_POPUP

POPUP ""[{leftBranch}]""
{leftFileMenu}
MENUITEM SEPARATOR
MENUITEM ""{leftRemote}"", em_git_status_left
END_POPUP

POPUP ""[{rightBranch}]""
{rightFileMenu}
MENUITEM SEPARATOR
MENUITEM ""{rightRemote}"", em_git_status_right
END_POPUP

HELP_BREAK";

            string filePath = @"c:\My\Run\Totalcmd\LANGUAGE\WCMD_ENG.MNU";
            string fileText = File.ReadAllText(filePath);
            fileText = Regex.Replace(fileText, search, replace, RegexOptions.Singleline);

            File.WriteAllText(filePath, fileText);
            
            //make TC refresh menu from its .LNG file
            Native.SendMessage(GetTcWindowHandle(), Native.WM_USER + 51, (IntPtr)2945, IntPtr.Zero);
        }

        public static string PathGetTail(string path)
        {
            var a = path.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            return a[a.Length - 1] + "\\";
        }

        public static void RemoveGitMenu()
        {
            string filePath = @"c:\My\Run\Totalcmd\LANGUAGE\WCMD_ENG.MNU";
            string search = @"STARTMENU.*HELP_BREAK";
            string replace = "STARTMENU\r\n\r\nHELP_BREAK";
            string fileText = File.ReadAllText(filePath);
            fileText = Regex.Replace(fileText, search, replace, RegexOptions.Singleline);
            File.WriteAllText(filePath, fileText);
            Native.SendMessage(GetTcWindowHandle(), Native.WM_USER + 51, (IntPtr)2945, IntPtr.Zero);
        }

        private string GetTcUserCmdFilePath()
        {
            object value = Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\Ghisler\Total Commander", "IniFileName", null);
            if (value == null)
            {
                DbgErr("TC registry key not found");
                return null;
            }

            string iniFilePath = value.ToString().ToLower();
            Dbg($"INI: {iniFilePath}");

            return iniFilePath.Replace("wincmd.ini", "usercmd.ini");
        }

        private string GetTcPanelPath(Side side)
        {
            IntPtr hWnd = GetTcWindowHandle();
            if (hWnd == IntPtr.Zero)
                return null;

            var text = new StringBuilder(1024);
            IntPtr hPanel = Native.SendMessage(hWnd, Native.WM_USER + 50, (IntPtr)side, IntPtr.Zero);
            Native.GetWindowTextW(hPanel, text, text.Capacity);
            string path = text.ToString();

            //trim trailing wildcards (from last \ till EOL)
            path = Regex.Replace(path, @"\\[^\\]+$", "\\");

            return path;
        }

        private static IntPtr GetTcWindowHandle() => Native.FindWindow("TTOTAL_CMD", null);

        private string GitGetBranch(string path)
        {
            (int exitCode, string branch) = Utils.ReadFromProcess(_gitExe, "rev-parse --abbrev-ref HEAD", path);
            if (exitCode != 0)
                branch = "";

            return branch;
        }

        private string GitGetModifiedFiles(string path)
        {
            (int exitCode, string modFiles) = Utils.ReadFromProcess(_gitExe, "status --porcelain", path);
            if (exitCode != 0)
            {
                Dbg($"status failed in {path}");
                return "";
            }

            //skip non-versions files marked with "?"
            modFiles = Regex.Replace(modFiles, @"^\?.+$", "", RegexOptions.Multiline);
            modFiles = Regex.Replace(modFiles, "\n+", "\n");
            modFiles = Regex.Replace(modFiles, "^ ", "", RegexOptions.Multiline);

            //count modified files
            string[] modFilesArray = modFiles.Trim().Split('\n');
            int modFilesCount = modFilesArray.Length;

            return modFiles;
        }

        private string[] GitGetModifiedFilesArray(string fileListFromGit)
        {
            if (string.IsNullOrWhiteSpace(fileListFromGit))
                return new string[0];

            string s = fileListFromGit;

            //skip non-versions files marked with "?"
            s = Regex.Replace(s, @"^\?.+$", "", RegexOptions.Multiline);
            s = Regex.Replace(s, "\n+", "\n");
            s = Regex.Replace(s, "^ ", "", RegexOptions.Multiline);

            return s.Trim().Split('\n');
        }

        private string GitGetRemote(string path)
        {
            (int exitCode, string remote) = Utils.ReadFromProcess(_gitExe, "remote -v", path);
            if (exitCode != 0)
            {
                Dbg($"no remote in {path}");
            }
            else
            {
                remote = Regex.Replace(remote, @"^.+\(push\)$", @"", RegexOptions.Multiline);
                remote = remote.Replace("(fetch)", "");
                remote = Regex.Replace(remote, "\n+", "\n");
                remote = remote.Trim();
            }

            return remote;
        }

        private class TCPanel
        {
            public string path;
            public string branch;
            public string remote;
            public string modFiles;
            public string[] modFilesArray;
            public int modFilesCount;

            public void Reset()
            {
                path = "";
                branch = "";
                remote = "";
                modFiles = "";
                modFilesArray = new []{""};
                modFilesCount = 0;
            }
        }

        private Dictionary<Side, TCPanel> _panels = new Dictionary<Side, TCPanel>()
        {
            [LEFT] = new TCPanel(),
            [RIGHT] = new TCPanel(),
        };

        public enum Side
        {
            //from TC HISTORY.TXT
            LEFT = 9,
            RIGHT = 10,
        };


        [SuppressUnmanagedCodeSecurity]
        internal class Native
        {
            public const int WM_USER = 0x0400;

            [DllImport("user32.dll")]
            public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

            [DllImport("user32.dll", SetLastError = true)]
            public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

            [DllImport("user32.dll")]
            public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

            [DllImport("user32.dll")]
            public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

            [DllImport("user32.dll")]
            public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

            [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern int GetWindowTextW(IntPtr hWnd, System.Text.StringBuilder lpString, int nMaxCount);
        }

    } //class
} //namespace
