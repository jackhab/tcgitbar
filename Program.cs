﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TcGitBar.Properties;
using static DebugLog.DebugLog;


namespace TcGitBar
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MyCustomApplicationContext());
        }
    }


    public class MyCustomApplicationContext : ApplicationContext
    {
        private NotifyIcon trayIcon;
        private FrmMain frmMain = new FrmMain();

        public MyCustomApplicationContext()
        {
            trayIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.outline_timeline_white_18dp,
                ContextMenu = new ContextMenu(new MenuItem[] { new MenuItem("Exit", Exit) }),
                BalloonTipText = "TCGit Bar",
                Visible = true,
            };

            Application.ApplicationExit += Exit;
            trayIcon.MouseClick += OnTrayIconMouseClick;
        }


        private void OnTrayIconMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                    frmMain.Show();
        }


        void Exit(object sender, EventArgs e)
        {
            //hide tray icon, otherwise it will remain shown until user mouses over it
            trayIcon.Visible = false;
            FrmMain.RemoveGitMenu();
            Application.Exit();
        }
    }
}
