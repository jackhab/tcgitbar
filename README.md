# README #

### What is this repository for? ###

This is a small git feature widget for Total Commander.

Working with a lot of git-versioned directories I needed something like git Bash prompt to show me checked out branch and status directly in Total Commander window.

So I made an AutoIt script which adds a small bar to the file path field which displays the current git branch, the number of changed files and a tooltip with a 'git status' output. Image: https://imgur.com/a/jf3646c

It requires AutoIt installation (alternatively it can be compiled into .exe by AutoIt).

If you click the bar it will try to open TortoiseGit since that's what I'm currently using.

Git executable folder must be in your PATH env. variable.

If you run TC as Administrator this script must also be launched as Administrator.

