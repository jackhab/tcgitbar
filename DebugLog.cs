﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.IO;


//in other classes: using static DebugLog.DebugLog

namespace DebugLog
{
    class DebugLog
    {
        public static Action<string> errorAction;

        private static Stopwatch sw = Stopwatch.StartNew();
        private static long elapsedMsPrev = 0;


        public static void Dbg(string message = "", 
            [CallerLineNumber] int lineNumber = 0, 
            [CallerMemberName] string caller = null,
            [CallerFilePath] string filePath = null, 
            bool isError = false)
        {
            string file = Path.GetFileName(filePath);
            string error = isError ? "Error!" : "";

            //print ms elapsed from start and from previous log
            var elapsedMs = sw.ElapsedMilliseconds;
            var elapsedMsDiff = elapsedMs - elapsedMsPrev;
            elapsedMsPrev = elapsedMs;

            Debug.WriteLine($">>{error} {message} [{file}:{caller}():{lineNumber}:{elapsedMsDiff}ms]");
        }


        public static void DbgErr(string message = "", 
            [CallerLineNumber] int lineNumber = 0, 
            [CallerMemberName] string caller = null,
            [CallerFilePath] string filePath = null)
        {
            Dbg(message, lineNumber, caller, filePath, isError: true);
            errorAction?.Invoke(message);
            //clear error message after delay
            Task.Delay(7000).ContinueWith(_ => errorAction?.Invoke(""));
        }


        public static string Escape(string input)
        {
            StringBuilder literal = new StringBuilder(input.Length + 2);
            literal.Append("\"");
            foreach (var c in input)
            {
                switch (c)
                {
                    //case '\'': literal.Append(@"\'"); break;
                    //case '\"': literal.Append("\\\""); break;
                    //case '\\': literal.Append(@"\\"); break;
                    case '\0': literal.Append(@"\0"); break;
                    case '\a': literal.Append(@"\a"); break;
                    case '\b': literal.Append(@"\b"); break;
                    case '\f': literal.Append(@"\f"); break;
                    case '\n': literal.Append(@"\n"); break;
                    case '\r': literal.Append(@"\r"); break;
                    case '\t': literal.Append(@"\t"); break;
                    case '\v': literal.Append(@"\v"); break;
                    default:
                        // ASCII printable character
                        if (c >= 0x20 && c <= 0x7e)
                        {
                            literal.Append(c);
                            // As UTF16 escaped character
                        }
                        else
                        {
                            literal.Append(@"\u");
                            literal.Append(((int)c).ToString("x4"));
                        }
                        break;
                }
            }
            literal.Append("\"");
            return literal.ToString();
        }
    }
}
