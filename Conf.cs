﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Reflection;
using System.Windows.Forms;
using static DebugLog.DebugLog;


namespace Conf
{
    public class Conf
    {
        public Conf()
        { 
        }

        public const string NOGITSTR = "---";
        public static void ConfSet(string key, string value)
        {
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.AppSettings.Settings;

                if (settings[key] == null)
                    settings.Add(key, value);
                else
                    settings[key].Value = value;

                config.Save();
                ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException e)
            {
                DbgErr($"Failed to write {key}:{value}. {e.Message}");
            }
        }

        public static string ConfGet(string key)
        {
            try
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;
                string[] arr = appSettings.GetValues(key);
                return (arr != null) ? arr[0] : "";
            }
            catch (Exception)
            {
                DbgErr($"Failed to read key {key}");
                return "";
            }
        }

    } //class
} //namespace
