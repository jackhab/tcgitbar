﻿using System.Windows.Forms;
using static Conf.Conf;


namespace TcGitBar
{
    public partial class FrmMain : Form
    {
        private void InitializeEvents()
        {
            btnBrowseGit.Click += OnBtnBrowseGitClick;
            btnBrowseTool.Click += OnBtnBrowseToolClick;
            btnOk.Click += OnBtnOkClick;
            btnCancel.Click += OnBtnCancelClick;
            numPeriod.ValueChanged += OnNumPeriodValueChanged;
            VisibleChanged += OnFrmMainVisibleChanged;
        }

        private void OnFrmMainVisibleChanged(object sender, System.EventArgs e)
        {
            _timer.Enabled = !this.Visible;
        }


        private void OnBtnBrowseToolClick(object sender, System.EventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Title = "Browse for External Tool Executable";
            dlg.InitialDirectory = @"C:\";
            dlg.Filter = "Executable|*.exe";
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtToolPath.Text = dlg.FileName;
                ConfSet(CONF_KEY_EXT_TOOL_EXE, dlg.FileName);
            }
        }

        private void OnBtnBrowseGitClick(object sender, System.EventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Title = "Browse for git.exe";
            dlg.InitialDirectory = @"C:\Program Files\Git\bin";
            dlg.Filter = "Git Executable|git.exe";
            dlg.FileName = "git.exe";
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtGitPath.Text = dlg.FileName;
                ConfSet(CONF_KEY_GIT_EXE, dlg.FileName);   
            }
        }

        private void OnNumPeriodValueChanged(object sender, System.EventArgs e)
        {
            _timer.Interval = (int)(numPeriod.Value * 1000);
        }

        private void OnBtnCancelClick(object sender, System.EventArgs e)
        {
            Hide();
        }

        private void OnBtnOkClick(object sender, System.EventArgs e)
        {
            Hide();
        }
    }
}
