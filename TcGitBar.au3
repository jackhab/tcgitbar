#include <Misc.au3>
#include <GuiConstants.au3>
#include <WinAPISysWin.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include "Common.au3"
#include <WinAPIGdi.au3>

;#AutoIt3Wrapper_Run_Debug_Mode=Y
_DebugSetup()

AutoItSetOption("MustDeclareVars", 1)
Opt("GUIOnEventMode", 1)

;#NoTrayIcon

Global Const $ON_CLICK_CMD = "C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe /command:log /path "

Global $hTcWin
Local $hLeftBarLabel
Local $hRightBarLabel

;misc vars for left and right bars
Local Enum $LEFT, $RIGHT
;the classes of TC controls to which bars are being attached to
Local $TcClasses[2] = ["[CLASSNN:Window13]", "[CLASSNN:Window18]"]
Local $Labels[2]
Local $Guis[2]
Local Const $BarWidth = 100

;keep window-active state between calls
Global $bWinActivated
Global $bWinWasActive


Func Main()
    While 1
        $hTcWin = WinWaitActive("[CLASS:TTOTAL_CMD]")

        ;crete GUIs, skip if TC was closed
        If Not CreateBar($LEFT) Then ContinueLoop
        If Not CreateBar($RIGHT) Then ContinueLoop

        While 1
            Sleep(300)

            $bWinActivated = $bWinWasActive <> WinActive($hTcWin) ? True : False
            $bWinWasActive = WinActive($hTcWin)

            ;update GUI position, skip if TC was closed
            If Not UpdatePosition($LEFT) Then ExitLoop
            If Not UpdatePosition($RIGHT) Then ExitLoop

            SetBar($LEFT)
            SetBar($RIGHT)
        WEnd

        GUIDelete($Guis[0])
        GUIDelete($Guis[1])
    Wend
EndFunc


Func UpdatePosition($Selector)
    ;move bars to new position in case TC window resize

	Static Local $WidthPrev = 0
	Local $aPos = WinGetPos(ControlGetHandle($hTcWin, "", $TcClasses[$Selector]))
    If @error Then Return False ;TC was closed
	
    If $aPos[2] <> $WidthPrev Then 
        Local $x = $aPos[2] - $BarWidth
        WinMove($Guis[$Selector], "", $x, 0)
        $WidthPrev = $aPos[2]
    EndIf
    Return True
EndFunc


Func CreateBar($Selector)
	Local $hTcPathField = ControlGetHandle($hTcWin, "", $TcClasses[$Selector])
	Local $aPos = WinGetPos($hTcPathField)
    If @error Then Return False ;TC was closed
	Local $y = 0
	Local $w = $BarWidth
	Local $h = $aPos[3]
	Local $x = $aPos[2] - $w

	;create bar GUIs
	Local $hBar = GUICreate("", $w, $h, $x, $y, BitOr($WS_POPUP, $WS_DLGFRAME), BitOr($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
	$Labels[$Selector] = GUICtrlCreateLabel("", 0, 0, $w, $h)
    GUICtrlSetOnEvent(-1, "OnClick" & $Selector)

    ;attach to TC window to follow its position and Z-order
	_WinAPI_SetParent($hBar, $hTcPathField)
	GUISetState(@SW_SHOW, $hBar)

    ;and save bar window handler for later use
	$Guis[$Selector] = $hBar
    Return True
EndFunc



Func OnClick0()
    OpenGitGui($LEFT)
EndFunc


Func OnClick1()
    OpenGitGui($RIGHT)
EndFunc


Func OpenGitGui($Side)
    Local $sPath = ControlGetText($hTcWin, "", $TcClasses[$Side])
    Run($ON_CLICK_CMD & $sPath)
    If WinWaitActive("- TortoiseGit", "", 300) Then
        Sleep(100)
        Send("{HOME}");
    EndIf
EndFunc


Func SetBar($Selector)
	;read path from TC
    Local $sPathFromTc = ControlGetText($hTcWin, "", $TcClasses[$Selector])

	;keep previous paths between calls for comparison
	Static Local $PrevPaths[2] = ["", ""]

	;update git status on either path or window-active change
	;the latter needed in case of branch switch
    If $bWinActivated Or ($sPathFromTc <> $PrevPaths[$Selector])  Then
		;trim trailing non-path chars
		Local $sPath = StringRegExpReplace($sPathFromTc, "\\[^\\]+$", "")

		;read git branch name
		Local $iPID = Run("git rev-parse --abbrev-ref HEAD", $sPath, @SW_HIDE, $STDOUT_CHILD)
		ProcessWaitClose($iPID)
        Local $bRepo = @extended == 0 ? True : False    ;is it a git repo?
		Local $sBranch = StdoutRead($iPID)
        $sBranch = StringStripWS($sBranch, $STR_STRIPALL)

        ;get a list of modified files
        If $bRepo Then
            $iPID = Run("git status --porcelain", $sPath, @SW_HIDE, $STDOUT_CHILD)
            ProcessWaitClose($iPID)
            Local $sOut= StdoutRead($iPID)

            ;skip non-versions files marked with "?"
            $sOut = StringRegExpReplace($sOut, "\?.+\n", "")

            ;count lines
            Local $aOut = StringSplit($sOut, @LF, $STR_ENTIRESPLIT)
            Local $i = UBound($aOut) - 2
            If $i <> 0 Then $sBranch &= " " & $i

            $iPID = Run("git remote get-url origin", $sPath, @SW_HIDE, $STDOUT_CHILD)
            ProcessWaitClose($iPID)
            Local $sRemote = StdoutRead($iPID)
            $sOut &= @LF & "from: " & $sRemote

            ;show git status in tool tip
            GUICtrlSetTip ($Labels[$Selector], $sOut)
        EndIf

		;set bar text
		GUICtrlSetData($Labels[$Selector], $sBranch)
		$PrevPaths[$Selector] = $sPathFromTc

        #cs
        ;cd /d works only in CMD, needs solution for powershell and bash
        ;'c:\My\Run\ConEmu\ConEmu\ConEmuC.exe -GuiMacro:0:T1:S1 keys("^c") ; sleep(100) ; print(@"cd /d ""' & $sPath &  '""") ; keys("Enter")'
        if $Selector == $LEFT Then
            Local $sCmd = 'c:\My\Run\ConEmu\ConEmu\ConEmuC.exe -GuiMacro:0:T1:S1 keys("^c") ; sleep(50) ; print(@"cd  ""' & $sPath &  '""") ; keys("Enter")'
        Else
            Local $sCmd = 'c:\My\Run\ConEmu\ConEmu\ConEmuC.exe -GuiMacro:0:T1:S2 keys("^c") ; sleep(50) ; print(@"cd  ""' & $sPath &  '""") ; keys("Enter")'
        EndIf

        If WinActive("[REGEXPTITLE:TC.]") Then
            ;Dbg($sCmd)
            Local $iPID = Run($sCmd, "", @SW_HIDE,  $RUN_CREATE_NEW_CONSOLE) ;$STDOUT_CHILD)
            ;Local $iPID = Run("dir", "", "",  $RUN_CREATE_NEW_CONSOLE) ;$STDOUT_CHILD)
            ProcessWaitClose($iPID)
            ;Local $s = StdoutRead($iPID)
            ;Dbg($s)
        EndIf
        #ce


	EndIf
EndFunc ;SetBar


Func GetBkColor($hWnd)
    If Not IsHWnd($hWnd) Then
        $hWnd = GUICtrlGetHandle($hWnd)
    EndIf
    Local $hDC = _WinAPI_GetDC($hWnd)
    Local $iColor = _WinAPI_GetPixel($hDC, 0, 0)
    _WinAPI_ReleaseDC($hWnd, $hDC)
    Return $iColor
EndFunc



Main()