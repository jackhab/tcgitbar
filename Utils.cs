﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using static DebugLog.DebugLog;


namespace TcGitBar
{
    class Utils
    {
        public static (int, string) ReadFromProcess(string command, string arguments, string workDir)
        {
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.FileName = command;
            proc.StartInfo.Arguments = arguments;
            proc.StartInfo.WorkingDirectory = workDir;
            proc.Start();

            var output = proc.StandardOutput.ReadToEndAsync();
            var error = proc.StandardError.ReadToEndAsync();
            proc.WaitForExit();
            var exitCode = proc.ExitCode;
            proc.Close();
            if (!string.IsNullOrWhiteSpace(error.Result))
                Dbg(error.Result);
            return (exitCode, output.Result.Trim());
        }


        public static void RunExe(string command, string arguments)
        {
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            //proc.StartInfo.CreateNoWindow = false;
            //proc.StartInfo.RedirectStandardOutput = true;
            //proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.FileName = command;
            proc.StartInfo.Arguments = arguments;

            try
            {
                proc.Start();
            }
            catch (Exception e)
            {
                Dbg(e.Message);
            }


            //var output = proc.StandardOutput.ReadToEndAsync();
            //var error = proc.StandardError.ReadToEndAsync();
            //proc.WaitForExit();
            //var exitCode = proc.ExitCode;
            //proc.Close();
            //if (!string.IsNullOrWhiteSpace(error.Result))
            //    Dbg(error.Result);
        }






        delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);
        WinEventDelegate onWinEvent = null;
        Action<IntPtr> onWinEventAction = null;

        [DllImport("user32.dll")]
        static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, uint idProcess, uint idThread, uint dwFlags);
        private const uint WINEVENT_OUTOFCONTEXT = 0;
        private const uint EVENT_SYSTEM_FOREGROUND = 3;

        //[DllImport("user32.dll")]
        //static extern IntPtr GetForegroundWindow();

        //[DllImport("user32.dll")]
        //static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        //private string GetActiveWindowTitle()
        //{
        //    const int nChars = 256;
        //    IntPtr handle = IntPtr.Zero;
        //    StringBuilder Buff = new StringBuilder(nChars);
        //    handle = GetForegroundWindow();

        //    if (GetWindowText(handle, Buff, nChars) > 0)
        //    {
        //        return Buff.ToString();
        //    }
        //    return null;
        //}

        public void WinEventProc(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            //Dbg(GetActiveWindowTitle());
            onWinEventAction(hwnd);
        }

        public void EnableWinEventHook(Action<IntPtr> action)
        {
            onWinEvent = new WinEventDelegate(WinEventProc);
            onWinEventAction = action;
            IntPtr m_hhook = SetWinEventHook(EVENT_SYSTEM_FOREGROUND, EVENT_SYSTEM_FOREGROUND, IntPtr.Zero, 
                onWinEvent, 0, 0, WINEVENT_OUTOFCONTEXT);
        }
    














} //class Utils
} //namespace
